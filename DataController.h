#ifndef DATACONTROLLER_H
#define DATACONTROLLER_H
#include<queue>
#pragma once
class DataController
{
public:
	DataController();
	~DataController();
	bool readFile(char*);
	const std::queue<char*> & getMessageQueue() const
	{
		return messageQueue;
	}
	static bool isQueueEmpty(std::queue<char*> messageQueue)
	{
		if (messageQueue.empty())
		{
			return true;
		}
		return false;
	}
private:
	std::queue<char*> messageQueue;

};

#endif