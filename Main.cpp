#include <iostream>
#include <queue>
#include "DataController.h"
using namespace std;

std::queue<char*> messageQueue;
int main()
{
	DataController dataC;

	dataC.readFile("Data.txt");

	messageQueue = dataC.getMessageQueue();

	while (!messageQueue.empty())
	{
		std::cout << messageQueue.front() << endl;
		messageQueue.pop();
	}

	char exit;
	cout << "......";
	cin >> exit;
	return 0;
}